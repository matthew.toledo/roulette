import java.util.Scanner;;

import org.graalvm.compiler.lir.aarch64.AArch64Move.StoreOp;

import jdk.javadoc.internal.doclets.formats.html.SourceToHTMLConverter;

public class Roulette {
    
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        RouletteWheel roul = new RouletteWheel();
        System.out.println("Would you like to place bet?");
        String response = keyboard.next();
        checkPlay(response);
    }

    public static int checkPlay(String response) {
        Scanner keyboard = new Scanner(System.in);
        if (response.equals("yes") || response.equals("Yes")) {
            System.out.println("How much would you like to bet?");
            int bet = keyboard.nextInt();
            return bet;
        } 
        else {
            return 0;
        }
    }


}
